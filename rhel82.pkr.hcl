variable "aws_access_key" {
  type    = string
  default = "${env("AWS_ACCESS_KEY")}"
}

variable "aws_secret_key" {
  type    = string
  default = "${env("AWS_SECRET_KEY")}"
}

locals { timestamp = regex_replace(timestamp(), "[- TZ:]", "") }


source "amazon-ebs" "rhel-82" {
  access_key    = "${var.aws_access_key}"
  ami_name      = "hardening-base-rhel82-${local.timestamp}"
  instance_type = "t2.micro"
  region        = "eu-west-2"
  secret_key    = "${var.aws_secret_key}"
  source_ami_filter {
    filters = {
      architecture        = "x86_64"
      name                = "RHEL-8.2.0_HVM-*"
      root-device-type    = "ebs"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["309956199498"] # Red Hat Inc.
  }
  ssh_username = "ec2-user"
}


build {
  sources = ["source.amazon-ebs.rhel-82"]

  provisioner "ansible" {
    playbook_file = "hardening.yml"
  }

  post-processor "manifest" {
    output     = "manifest.json"
    strip_path = true
    custom_data = {
      base_os = "RHEL-8.2"
    }
  }
}
