# Build a hardened RHEL-8.2 image on AWS with Packer

Use Packer to find Red Hat Enterprise Linux 8.2 x86_64 image, run the Ansible roles provided by [DevSec Hardening Famerwork](https://dev-sec.io/) and bake a custom hardened RHEL-8.2 image for provisioning VMs in the cloud.



```bash
$ packer version
Packer v1.6.6

$ ansible --version
ansible [core 2.11.5] 
  config file = None
  configured module search path = ['$HOME/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = $HOME/.local/lib/python3.9/site-packages/ansible
  ansible collection location = $HOME/.ansible/collections:/usr/share/ansible/collections
  executable location = $HOME/.local/bin/ansible
  python version = 3.9.2 (default, Feb 28 2021, 17:03:44) [GCC 10.2.1 20210110]
  jinja version = 3.0.1
  libyaml = True

$ aws --version
aws-cli/2.1.30 Python/3.8.8 Linux/5.10.0-11-amd64 exe/x86_64.debian.11 prompt/off

```

### Step 1 - download the roles from Ansible Galaxy

```bash
$ ansible-galaxy install -r requirements.yml
```

### Step 2 - create and validate your Packer configuration

```bash
$ packer fmt rhel82.pkr.hcl && packer validate rhel82.pkr.hcl 
```

### Step 3 - build your image

```bash
$ packer build rhel82.pkr.hcl 
```

### Step 4 - validate your image using AWS CLI
```bash
$ aws ec2 describe-images --image-ids ami-EXAMPLE1234
{
    "Images": [
        {
            "Architecture": "x86_64",
            "CreationDate": "2022-01-30T09:48:38.000Z",
            "ImageId": "ami-EXAMPLE1234",
            "ImageLocation": "249898599466/hardening-base-rhel82-20220130094345",
            "ImageType": "machine",
            "Public": false,
            "OwnerId": "EXAMPLE1234",
            "PlatformDetails": "Red Hat Enterprise Linux",
            "UsageOperation": "RunInstances:0010",
            "State": "available",
            "BlockDeviceMappings": [
                {
                    "DeviceName": "/dev/sda1",
                    "Ebs": {
                        "DeleteOnTermination": true,
                        "SnapshotId": "snap-EXAMPLE1234",
                        "VolumeSize": 10,
                        "VolumeType": "gp2",
                        "Encrypted": false
                    }
                }
            ],
            "EnaSupport": true,
            "Hypervisor": "xen",
            "Name": "hardening-base-rhel82-20220130094345",
            "RootDeviceName": "/dev/sda1",
            "RootDeviceType": "ebs",
            "SriovNetSupport": "simple",
            "VirtualizationType": "hvm"
        }
    ]
}
```

### Step 5 - clean up

If you ran this project for study purposes, make sure that you deregister the AMI and delete the snapshot on AWS otherwise you may get charged for them, depending on your AWS plan.
